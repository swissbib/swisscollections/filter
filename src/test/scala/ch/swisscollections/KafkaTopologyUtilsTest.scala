/*
 * filter
 * Copyright (C) 2020  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.swisscollections

import org.scalatest.funsuite.AnyFunSuite

import scala.io.Source
import scala.xml.{SAXParseException, XML}

class KafkaTopologyUtilsTest extends AnyFunSuite {

  private def getTextFromResourcesFolder(filepath: String): String = {
    val file = Source.fromFile(s"src/test/resources/$filepath")
    val text = file.mkString
    file.close()
    text
  }

  test("Test get subfield") {
    val xmlString = getTextFromResourcesFolder("record/han_record.xml")
    val xml = XML.loadString(xmlString)

    val subfield = KafkaTopologyUtils.getSubfields(xml, "900", "a").toString()
    assert (subfield =="<subfield code=\"a\">HANunikat</subfield>")
  }

  test("Test get leader 06") {
    val xmlString = getTextFromResourcesFolder("record/han_record.xml")
    val xml = XML.loadString(xmlString)

    val leader06 = KafkaTopologyUtils.getLeader06Field(xml)
    assert (leader06 =="t")
  }

  test("Test field contains (with header)") {
    val xmlString = getTextFromResourcesFolder("record/han_record_with_header.xml")
    val xml = XML.loadString(xmlString)
    val recordWithoutHeader = (xml \ "metadata" \ "record").toString()
    val recordXml = XML.loadString(recordWithoutHeader)
    val result = KafkaTopologyUtils.fieldContains(recordXml, "900", "a", "HANunikat")
    assert (result)

    val xmlString2 = getTextFromResourcesFolder("record/not_han_record_with_header.xml")
    val xml2 = XML.loadString(xmlString2)
    val recordWithoutHeader2 = (xml2 \ "metadata" \ "record").toString()
    val recordXml2 = XML.loadString(recordWithoutHeader2)
    val result2 = KafkaTopologyUtils.fieldContains(recordXml2, "900", "a", "HANunikat")
    assert (!result2)

    val xmlString3 = getTextFromResourcesFolder("record/other_record_with_header.xml")
    val xml3 = XML.loadString(xmlString3)
    val recordWithoutHeader3 = (xml3 \ "metadata" \ "record").toString()
    val recordXml3 = XML.loadString(recordWithoutHeader3)
    val result3 = KafkaTopologyUtils.fieldContains(recordXml3, "852", "b", "Z01")
    assert (result3)

    val xmlString4 = getTextFromResourcesFolder("record/record_with_multiple_locations.xml")
    val xml4 = XML.loadString(xmlString4)
    val recordWithoutHeader4 = (xml4 \ "metadata" \ "record").toString()
    val recordXml4 = XML.loadString(recordWithoutHeader4)
    val result4 = KafkaTopologyUtils.fieldContains(recordXml4, "852", "b", "Z02")
    assert (result4)
    val result41 = KafkaTopologyUtils.fieldContains(recordXml4, "852", "b", "02")
    assert (result41)
    val result42 = KafkaTopologyUtils.fieldContains(recordXml4, "852", "b", "Z03")
    assert (!result42)
    val result43 = KafkaTopologyUtils.fieldStartsWith(recordXml4, "852", "b", "Z02")
    assert (result43)
    val result44 = KafkaTopologyUtils.fieldStartsWith(recordXml4, "852", "b", "Z0")
    assert (result44)
    val result45 = KafkaTopologyUtils.fieldStartsWith(recordXml4, "852", "b", "02")
    assert (!result45)
    val result46 = KafkaTopologyUtils.fieldIsEqual(recordXml4, "852", "b", "Z02")
    assert (result46)
    val result47 = KafkaTopologyUtils.fieldIsEqual(recordXml4, "852", "b", "Z0")
    assert (!result47)
  }

  test("Exceptions") {
    val xmlString = getTextFromResourcesFolder("record/han_record_with_header_wrong.xml")
    val res = KafkaTopologyUtils.shouldAppearInSwissCollections(xmlString)
    assert(!res)

    val xmlString2 = getTextFromResourcesFolder("record/han_record_with_header_wrong2.xml")
    val res2 = KafkaTopologyUtils.shouldAppearInSwissCollections(xmlString2)
    assert(!res2)


  }

  test("Verweisungen") {
    val xmlString = getTextFromResourcesFolder("record/record_verweisungen_zb.xml")
    val res = KafkaTopologyUtils.shouldAppearInSwissCollections(xmlString)
    assert(!res)
  }

  test("Years") {
    val xmlString = getTextFromResourcesFolder("record/not_han_record_with_header.xml")
    val xml = XML.loadString(xmlString)
    val recordWithoutHeader = (xml \ "metadata" \ "record").toString()
    val recordXml = XML.loadString(recordWithoutHeader)
    val res = KafkaTopologyUtils.getFirstYear(recordXml).getOrElse("unknown")
    assert(res == 1400)
    assert(KafkaTopologyUtils.publishedBefore(recordXml, 1450))
    assert(KafkaTopologyUtils.publishedBefore(recordXml, 1400))
    assert(!KafkaTopologyUtils.publishedBefore(recordXml, 1399))

  }

  test("Year - 2nd test") {
    val xmlString = getTextFromResourcesFolder("record/year_unknown.xml")
    val xml = XML.loadString(xmlString)
    val recordWithoutHeader = (xml \ "metadata" \ "record").toString()
    val recordXml = XML.loadString(recordWithoutHeader)
    val res = KafkaTopologyUtils.getFirstYear(recordXml).getOrElse("unknown")
    assert(res == "unknown")
  }

  test("is from zb") {
    val xmlString = getTextFromResourcesFolder("record/record_verweisungen_zb.xml")
    val xml = XML.loadString(xmlString)
    val recordWithoutHeader = (xml \ "metadata" \ "record").toString()
    val recordXml = XML.loadString(recordWithoutHeader)
    assert(KafkaTopologyUtils.isFromZb(recordXml))

    val xmlString2 = getTextFromResourcesFolder("record/year_unknown.xml")
    val xml2 = XML.loadString(xmlString2)
    val recordWithoutHeader2 = (xml2 \ "metadata" \ "record").toString()
    val recordXml2 = XML.loadString(recordWithoutHeader2)

    assert(!KafkaTopologyUtils.isFromZb(recordXml2))
  }

  test("Before 1899 + special zb rules") {
    //val xmlString = getTextFromResourcesFolder("record/zb_special_year1.xml")
    //val res = KafkaTopologyUtils.shouldAppearInSwissCollections(xmlString)
    //assert(res == true)

    val xmlString2 = getTextFromResourcesFolder("record/zb_special_year2.xml")
    val res2 = KafkaTopologyUtils.shouldAppearInSwissCollections(xmlString2)
    assert(!res2)

    val xmlString3 = getTextFromResourcesFolder("record/zb_special_year3.xml")
    val res3 = KafkaTopologyUtils.shouldAppearInSwissCollections(xmlString3)
    assert(res3)
  }

  test("analytica image zb") {
    val xmlString = getTextFromResourcesFolder("record/zb_analytica_image.xml")
    val res = KafkaTopologyUtils.shouldAppearInSwissCollections(xmlString)
    assert(res)
  }

  test("should be deleted") {
    val xmlString = getTextFromResourcesFolder("record/han_deleted_record.xml")
    assert(KafkaTopology.shouldBeDeleted(xmlString))

  }

  test("document ist not deleted") {
    val xmlString = getTextFromResourcesFolder("record/han_record_with_header.xml")
    assert(!KafkaTopology.shouldBeDeleted(xmlString))

  }

  test("year 18uu") {
    val xmlString = getTextFromResourcesFolder("record/han_record_year18??.xml")
    val xml = XML.loadString(xmlString)
    val recordWithoutHeader = (xml \ "metadata" \ "record").toString()
    val recordXml = XML.loadString(recordWithoutHeader)


    val res1 = KafkaTopologyUtils.getFirstYear(recordXml).getOrElse("unknown")
    assert(res1 == 1800)

    val res2 = KafkaTopologyUtils.shouldAppearInSwissCollections(xmlString)
    assert(res2)
  }

  test("zb drehbuch") {
    val xmlString = getTextFromResourcesFolder("record/zb_drehbuch.xml")
    val res = KafkaTopologyUtils.shouldAppearInSwissCollections(xmlString)
    assert(res)
  }

  test ("HANunikat from A332 Musikakademie") {
    val xmlString = getTextFromResourcesFolder("record/han_record_from_A332.xml")
    val res = KafkaTopologyUtils.shouldAppearInSwissCollections(xmlString)
    assert(!res)
  }

  test ("Zurich bibliography") {
    val xmlString = getTextFromResourcesFolder("record/zh_bibliography.xml")
    val res = KafkaTopologyUtils.shouldAppearInSwissCollections(xmlString)
    assert(res)
  }

  test ("Keller bibliography") {
    val xmlString = getTextFromResourcesFolder("record/zb_keller.xml")
    val res = KafkaTopologyUtils.shouldAppearInSwissCollections(xmlString)
    assert(res)

    val xmlString2 = getTextFromResourcesFolder("record/zb_keller_signatur.xml")
    val res2 = KafkaTopologyUtils.shouldAppearInSwissCollections(xmlString2)
    assert(res2)
  }

  test ("hide this in 990") {
    val xmlString = getTextFromResourcesFolder("record/hide_this_990.xml")
    val res = KafkaTopologyUtils.shouldAppearInSwissCollections(xmlString)
    assert(!res)
  }

  test ("hanunikat no bib") {
    val xmlString = getTextFromResourcesFolder("record/no_han_library.xml")
    val res = KafkaTopologyUtils.shouldAppearInSwissCollections(xmlString)
    assert(!res)
  }

  test ("SOB") {
    val xmlString = getTextFromResourcesFolder("record/sob_record.xml")
    val res = KafkaTopologyUtils.shouldAppearInSwissCollections(xmlString)
    assert(res)
  }

  test ("deleted") {
    val xmlString = getTextFromResourcesFolder("record/deleted.xml")
    val res = KafkaTopologyUtils.shouldAppearInSwissCollections(xmlString)
    assert(!res)
  }

  test("analytica no inventory") {
    val xmlString = getTextFromResourcesFolder("record/analytica-998.xml")
    val res = KafkaTopologyUtils.shouldAppearInSwissCollections(xmlString)
    assert(res)
  }

  test ("ag bibliography") {
    val xmlString = getTextFromResourcesFolder("record/ag_bibliography.xml")
    val res = KafkaTopologyUtils.shouldAppearInSwissCollections(xmlString)
    assert(res)
  }


}
