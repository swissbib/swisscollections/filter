/*
 * filter
 * Copyright (C) 2020  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.swisscollections

import ch.swisscollections.KafkaTopology.shouldBeDeleted
import org.apache.kafka.streams.Topology
import org.apache.kafka.streams.scala.ImplicitConversions._
import org.apache.kafka.streams.scala.{StreamsBuilder, _}
import org.apache.logging.log4j.scala.Logging

import scala.util.{Failure, Success, Try}
import scala.xml.{Elem, XML}

class KafkaTopology extends Logging {

  import KafkaTopologyUtils._
  import Serdes._


  def build(): Topology = {
    val builder = new StreamsBuilder

    val source = builder.stream[String, String](SettingsFromFile.getKafkaInputTopic)

    val Array(toDelete, checkFilterRules) = source
      .branch(
        (_, v) => shouldBeDeleted(v),
        (_, _) => true
      )


    val filteredDocuments = checkFilterRules.filter(
      (_, value) => shouldAppearInSwissCollections(value)
    )


    filteredDocuments.to(SettingsFromFile.getKafkaOutputTopic)
    toDelete.to(SettingsFromFile.getKafkaOutputTopic)

    builder.build()
  }




}


object KafkaTopology extends Logging {
  val shouldBeDeleted: String => Boolean =
    sourceRecord => {
      Try {
        XML.loadString(sourceRecord)
      } match {
        case Success(xml) => (xml \ "header" \ "@status" ).text == "deleted"
        case Failure(exception) =>
          logger.error(exception.getMessage)
          false
      }


    }

}
