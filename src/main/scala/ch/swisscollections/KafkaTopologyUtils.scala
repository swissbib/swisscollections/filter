/*
 * filter
 * Copyright (C) 2020  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.swisscollections

import org.apache.logging.log4j.scala.Logging

import scala.util.{Failure, Success, Try}
import scala.xml.{Elem, Node, NodeSeq, SAXParseException, XML}

object KafkaTopologyUtils extends Logging {


  def shouldAppearInSwissCollections(record: String): Boolean ={
    try {

      val xml: Elem = XML.loadString(record)

      val recordWithoutHeader = (xml \ "metadata" \ "record").toString()
      val recordXml = XML.loadString(recordWithoutHeader)
      if(fieldContains(recordXml, "900", "f", "HANhide_this")) {
        false
      }
      else if(fieldStartsWith(recordXml, "990", "f", "hide_this")) {
        false
      }
      else if(isFromLibrary(recordXml, "Z05") && fieldIsEqual(recordXml, "245", "a", "[Verweisung]")) {
        false
      }
      else if(
        //Basler Bibliographie
        fieldStartsWith(recordXml, "990", "b", "basb") ||

        //Rossica-Bestände der Schweizerischen Osteuropabibliothek
        fieldStartsWith(recordXml, "990", "f", "rossica") ||

        //Berner Bibliographie
        fieldStartsWith(recordXml, "990", "b", "bbg") ||

        //Zürcher Bibliographie
        fieldStartsWith(recordXml, "990", "a", "zuebi") ||

        //nicht kodierte Nachlassbibliotheken
        fieldContains(recordXml, "990", "f", "NLBurckhardtLuc") ||
        fieldContains(recordXml, "990", "f", "BibJBu") ||
          (
            // Gelegenheitsschriften aus Staatsarchiv Basel (Kooperation mit UB Basel)
            // Aus A348 muss man nur die IDSIxkasualia importieren
            fieldIsEqual(recordXml, "900", "f", "IDSIxkasualia") &&
            isFromLibrary(recordXml, "A348")
          ) ||
          (
            // alle Bücher aus der Abteilung
            // Alte Drucke und Rara Z06
            // aber nicht die Signaturen der Handbibliothek im
            // Lesesaal LAD und HB LAD]
            getLeader06Field(recordXml) == "a" && isFromLibrary(recordXml, "Z06") && !(fieldStartsWith(recordXml, "852", "j", "LAD") || fieldStartsWith(recordXml, "852", "j", "HB LAD"))
          ) ||
          // Verlagsbucharchiv ist voll importiert
          isFromLibrary(recordXml, "Z08") ||

          //Sammlung der Schweizerische Drehbücher der ZB Zürich
          getLeader06Field(recordXml) == "a" && isFromLibrary(recordXml, "Z05") && fieldStartsWith(recordXml, "852", "j", "MFSD") ||

          // Gottfried-Keller Bibliographie
          (fieldContains(recordXml, "900", "a", "IDSZ5gkn") ||
            fieldContains(recordXml, "900", "a", "IDSZ1GK") ||
            fieldContains(recordXml, "990", "a", "GK_Z01") ||
            (isFromZb(recordXml) && (fieldStartsWith(recordXml, "852", "j", "42") ||
              fieldStartsWith(recordXml, "852", "j", "43") ||
              fieldStartsWith(recordXml, "852", "j", "Ms GK") ||
              fieldStartsWith(recordXml, "852", "j", "GK") ||
              fieldStartsWith(recordXml, "852", "j", "GKN")))
            ) ||

          // Rossica, SOB
          (isFromLibrary(recordXml, "B415") && (fieldStartsWith(recordXml, "852", "j", "SOB 882 D") ||
            fieldStartsWith(recordXml, "852", "j", "SOB RRA") ||
            fieldStartsWith(recordXml, "852", "j", "SOB RRD") ||
            fieldStartsWith(recordXml, "852", "j", "SOB RRE") ||
            fieldStartsWith(recordXml, "852", "j", "SOB RRZ") ||
            fieldStartsWith(recordXml, "949", "h", "SOB 882 D") ||
            fieldStartsWith(recordXml, "949", "h", "SOB RRA") ||
            fieldStartsWith(recordXml, "949", "h", "SOB RRD") ||
            fieldStartsWith(recordXml, "949", "h", "SOB RRE") ||
            fieldStartsWith(recordXml, "949", "h", "SOB RRZ"))
            ) ||

          // Solothurner Bibliographie
          fieldStartsWith(recordXml, "990", "a", "sobib") ||
          fieldStartsWith(recordXml, "990", "b", "sobib") ||

          // Aargauer Bibliographie
          (fieldStartsWith(recordXml, "990", "a", "AG") && getControlfield(recordXml, "001").nonEmpty && getControlfield(recordXml, "001").text.takeRight(4) == "8281") ||

          // UB Basel CallNumbers
          (isFromLibrary(recordXml, "A100") && (fieldStartsWith(recordXml, "852", "j", "UBH NL ") ||
            fieldStartsWith(recordXml, "949", "h", "UBH NL ") ||
            fieldStartsWith(recordXml, "852", "j", "UBH Pal ") ||
            fieldStartsWith(recordXml, "949", "h", "UBH Pal ") ||
            fieldStartsWith(recordXml, "852", "j", "UBH AN ") ||
            fieldStartsWith(recordXml, "949", "h", "UBH AN ") ||
            fieldStartsWith(recordXml, "852", "j", "UBH HHss Cv ") ||
            fieldStartsWith(recordXml, "949", "h", "UBH HHss Cv ") ||
            fieldStartsWith(recordXml, "852", "j", "UBH BUB ") ||
            fieldStartsWith(recordXml, "949", "h", "UBH BUB ") ||
            fieldStartsWith(recordXml, "852", "j", "UBH ISR ") ||
            fieldStartsWith(recordXml, "949", "h", "UBH ISR "))
            ) ||
          //criterions which are coupled to a library code :
          (
            (
              fieldContains(recordXml, "900", "a", "HANunikat")
                || fieldContains(recordXml, "900", "a", "IDSIxunikat")
                || fieldContains(recordXml, "900", "a", "unikat")
                || fieldIsEqual(recordXml, "900", "a", "IDSIxkasualia")
                || getLeader06Field(recordXml) == "j" //ton
                || getLeader06Field(recordXml) == "c" //musikalien
                || getLeader06Field(recordXml) == "d" //musikalien
                || getLeader06Field(recordXml) == "e" //karten
                || getLeader06Field(recordXml) == "f" //karten
                || (getLeader06Field(recordXml) == "g" && getControlfield(recordXml, "007").nonEmpty && getControlfield(recordXml, "007").text.substring(0,1) == "g") //bildmaterialien
                || getLeader06Field(recordXml) == "k" //bildmaterialien
                || getLeader06Field(recordXml) == "r" //bildmaterialien
                || getLeader06Field(recordXml) == "t" //handschriften
                || getLeader06Field(recordXml) == "a" && publishedBefore(recordXml, 1899) //bücher vor 1899
            ) && (
              isFromLibrary(recordXml, "AKB")
                || isFromLibrary(recordXml, "A382")
                || isFromLibrary(recordXml, "SGARK")
                || isFromLibrary(recordXml, "A100")
                || isFromLibrary(recordXml, "A115")
                || isFromLibrary(recordXml, "A116")
                || isFromLibrary(recordXml, "A117")
                || isFromLibrary(recordXml, "A118")
                || isFromLibrary(recordXml, "A125")
                || isFromLibrary(recordXml, "A380")
                || isFromLibrary(recordXml, "SGKBV")
                || isFromLibrary(recordXml, "SGSTI")
                || isFromLibrary(recordXml, "A381")
                || isFromLibrary(recordXml, "Z01")
                || isFromLibrary(recordXml, "Z02")
                || isFromLibrary(recordXml, "Z03")
                || isFromLibrary(recordXml, "Z04")
                || isFromLibrary(recordXml, "Z05")
                || isFromLibrary(recordXml, "Z06")
                || isFromLibrary(recordXml, "Z08")
                || isFromLibrary(recordXml, "UMWI")
                || isFromLibrary(recordXml, "E30")
                || isFromLibrary(recordXml, "B400")
                || isFromLibrary(recordXml, "B404")
                || isFromLibrary(recordXml, "B410")
                || isFromLibrary(recordXml, "B412")
                || isFromLibrary(recordXml, "B415")
                || isFromLibrary(recordXml, "B452")
                || isFromLibrary(recordXml, "B464")
                || isFromLibrary(recordXml, "B465")
                || isFromLibrary(recordXml, "B466")
                || isFromLibrary(recordXml, "B467")
                || isFromLibrary(recordXml, "B500")
                || isFromLibrary(recordXml, "B521")
                || isFromLibrary(recordXml, "B542")
                || isFromLibrary(recordXml, "B552")
                || isFromLibrary(recordXml, "B554")
                || isFromLibrary(recordXml, "B555")
                || isFromLibrary(recordXml, "B583")
                || isFromLibrary(recordXml, "B589")
                || isFromLibrary(recordXml, "LUSBI")
                || isFromLibrary(recordXml, "LUZHB")
                || isFromLibrary(recordXml, "A150")
                || fieldIsEqual(recordXml, "998", "a", "no_inventory_analytical")
            )
          )
      ) {
        true
      } else {
        false
      }
    } catch {
      case e: SAXParseException => {
        logger.error(s"record $record throws SAX exception ${e.getMessage}")
        e.printStackTrace
        false
      }
      case e: java.lang.StringIndexOutOfBoundsException => {
        logger.error(s"record $record throws exception ${e.getMessage}")
        e.printStackTrace
        false
      }
    }
  }



  /*
  Check if any of the specified marc field/subfield contains the text text. If yes return true. Else, false.
  Case Insensitive
   */
  def fieldContains(recordXml: Elem, field: String, subfield: String, text: String): Boolean = {

    val subfields = getSubfields(recordXml, field, subfield)

    var result = false

    for (subfield <- subfields if subfield.text.toLowerCase.contains(text.toLowerCase))
      result=true
    result
  }

  /*
  Check if any of the marc field/subfield starts with the text text. If yes return true. Else, false.
  Case insensitive
   */
  def fieldStartsWith(recordXml: Elem, field: String, subfield: String, text: String): Boolean = {

    val subfields = getSubfields(recordXml, field, subfield)

    var result = false

    for (subfield <- subfields if subfield.text.toLowerCase.startsWith(text.toLowerCase))
      result = true
    result
  }

  /*
  Check if any of the marc field/subfield is the same as the text text. If yes return true. Else, false.
  Case insensitive
   */
  def fieldIsEqual(recordXml: Elem, field: String, subfield: String, text: String): Boolean = {

    val subfields = getSubfields(recordXml, field, subfield)

    var result = false

    for (subfield <- subfields if subfield.text.toLowerCase == text.toLowerCase)
      result = true
    result
  }

  //get the position 06 of the leader field
  def getLeader06Field(recordXml: Elem): String = {
      (recordXml \\ "leader").text.substring(6,7)
  }

  def isFromLibrary(recordXml: Elem, libraryCode: String): Boolean =
  {
    if(fieldIsEqual(recordXml, "852", "b", libraryCode)) {
      true
    } else if (fieldIsEqual(recordXml, "AVD", "g", libraryCode)) {
      true
    } else {
      false
    }
  }

  //returns a sequence of the corresponding subfields
  def getSubfields(rec: Node, field: String, subfield: String): NodeSeq = {
    for {
      df <- rec \\ "datafield"
      if df \@ "tag" == field
      sf <- df \\ "subfield"
      if sf \@ "code" == subfield
    } yield sf
  }

  //returns a sequence of the corresponding subfields
  def getControlfield(rec: Node, controlfield: String): NodeSeq = {
    for {
      df <- rec \\ "controlfield"
      if df \@ "tag" == controlfield
    } yield df
  }

  //gets year out of 008 (year 1)
  def getFirstYear(recordXml: Elem): Option[Int] = {
    try {
      val controlfield008 = getControlfield(recordXml, "008")
      Some(controlfield008.text.substring(7, 11).replace('u','0').toInt)
    } catch {
      case e: java.lang.NumberFormatException => {
        None
      }
      case e: java.lang.StringIndexOutOfBoundsException => {
        None
      }

    }
  }

  def publishedBefore(recordXml: Elem, year: Int): Boolean = {
    val test = getFirstYear(recordXml)
    getFirstYear(recordXml) match {
      case Some(i) => (i <= year)
      case None => false
    }
  }

  def isFromZb(recordXml: Elem): Boolean = {
    if(isFromLibrary(recordXml, "Z01") ||
      isFromLibrary(recordXml, "Z02") ||
      isFromLibrary(recordXml, "Z03") ||
      isFromLibrary(recordXml, "Z04") ||
      isFromLibrary(recordXml, "Z05") ||
      isFromLibrary(recordXml, "Z06") ||
      isFromLibrary(recordXml, "Z08")) {
      true
    } else {
      false
    }
  }

}
