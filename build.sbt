import Dependencies._

ThisBuild / resolvers ++= Seq(
  "marcxml-fields" at "https://gitlab.com/api/v4/projects/12974592/packages/maven",
  Resolver.mavenLocal
)


ThisBuild / scalaVersion := "2.13.15"
ThisBuild / organization := "ch.swisscollections"
ThisBuild / organizationName := "swisscollections"
ThisBuild / git.gitTagToVersionNumber := { tag: String =>
  if (tag matches "[0-9]+\\..*") {
    Some(tag)
  }
  else {
    None
  }
}


lazy val root = (project in file("."))
  .enablePlugins(GitVersioning)
  .settings(
    name := "Filter",
    assemblyJarName in assembly := "app.jar",
    test in assembly := {},
    assemblyMergeStrategy in assembly := {
      case "log4j.properties" => MergeStrategy.first
      case "log4j2.xml" => MergeStrategy.first
      case other if other.contains("module-info.class") => MergeStrategy.discard
      case x =>
        val oldStrategy = (assemblyMergeStrategy in assembly).value
        oldStrategy(x)
    },
    mainClass in assembly := Some("ch.swisscollections.App"),
    resolvers ++= Seq(
      "Memobase Libraries" at "https://gitlab.switch.ch/api/v4/projects/1324/packages/maven"
    ),
    libraryDependencies ++= Seq(
      kafkaStreams,
      log4jApi,
      log4jCore,
      log4jSlf4j,
      log4jScala,
      memobaseServiceUtils excludeAll (ExclusionRule(organization =
        "org.slf4j"
      )),
      scalatic,
      uPickle,
      kafkaStreamsTestUtils % Test,
      scalaTest % Test,
      scalaxml)
  )
